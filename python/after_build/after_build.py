import os, time,sys
from time import gmtime, strftime
import shutil
import hashlib
import re
import zipfile
import argparse
import glob


# ################################################################################
# Build utility
# ################################################################################

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def find_line(file,keyword):
    src_file = file
    found_line = ""
    with open(src_file) as old_file:
        for line in old_file:
            if keyword in line:
                found_line = line
                break

    return (found_line)

def find_lines(file,keywords):
    src_file = file
    found_line = []
    with open(src_file) as old_file:
        for line in old_file:
            for keyword in keywords:
                if keyword in line:
                   found_line.append(line)

    return (found_line)


def find_latest_file(path,extensions):
#    print ("find_latest_file",path,extensions)
    mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
    sortedlist = list(sorted(os.listdir(path), key=mtime,reverse=True))
    file_names = [fn for fn in sortedlist if any([fn.endswith(extensions)])]
    return (file_names[0])

# ################################################################################
# Build utility
# ################################################################################

def run_shell(cmd):
    retStr = (os.popen(cmd).read()).replace("\n","")
    return (retStr)


def gen_sys_cfg(args):

    cfg={}
    cfg["version"] = "00.00.01"
    cfg["python3"] = "C:\\python3\\python "
    cfg["compiler"] = args.c
    cfg["hw_rev"] = args.h
    cfg["bl"] = args.b
    cfg["backup"] = args.z
    cfg["header"] = args.a
    cfg["git"] = args.g
    cfg["lang"] = args.l
    cfg["uv4"] = args.u
    cfg["timestamp"] = time.strftime("%y%m%d_%H%M%S")

    return (cfg)

def gen_prj_cfg():

    cfg={}
    cfg["path_project"] = os.getcwd().replace("keil","")
    cfg["path_build"] = os.getcwd() + '\\bin'
    cfg["path_local_release"] = os.getcwd() + "\\release"
    cfg["build_extension"] = '.axf'
    cfg["build_file"] = find_latest_file(cfg["path_build"], cfg["build_extension"])
    cfg["bin_file"] = cfg["build_file"].replace(cfg["build_extension"],".bin")
    cfg["output_file"] = cfg["build_file"].replace(cfg["build_extension"],"")


    cfg["build_log"] = find_latest_file(cfg["path_build"] , ".build_log.htm")
    cfg["prj_file"] = find_line(cfg["path_build"] + "\\" +  cfg["build_log"], ".uvprojx").replace("\n","")
    cfg["option"] = find_keil_prj_option(cfg["prj_file"] )

    cfg["build_option"] = cfg["option"]["build_option"]


    cfg["language_pack"] = "EN"

    if " LANG_EN" in cfg["build_option"]:
        cfg["language_pack"] = "EN"

    if " LANG_DE" in cfg["build_option"]:
        cfg["language_pack"] = "DE"

    if " LANG_ES" in cfg["build_option"]:
        cfg["language_pack"] = "ES"

    if " LANG_IT" in cfg["build_option"]:
        cfg["language_pack"] = "IT"

    if " LANG_FR" in cfg["build_option"]:
        cfg["language_pack"] = "FR"

    if " LANG_PT" in cfg["build_option"]:
        cfg["language_pack"] = "PT"

    if " LANG_CN" in cfg["build_option"]:
        cfg["language_pack"] = "CN"

    if " LANG_JP" in cfg["build_option"]:
        cfg["language_pack"] = "JP"

    if " LANG_KR" in cfg["build_option"]:
        cfg["language_pack"] = "KR"


#    print (cfg)
    return (cfg)


def gen_output_cfg():
    cfg={}
    cfg["fromelf"] = "C:\\Keil_v5\\ARM\\ARMCC\\bin\\fromelf.exe"
    cfg["make_path"] = "ota_bootloader"
    cfg["bootloader_combine"] = "bootloader_binary_combiner.py"
    cfg["ota_convert"] = "ota_binary_converter.py"
    cfg["ota_combine_flag"] = " --flag-addr 0x6000 --load-address 0x6800 "
    cfg["ota_load_flag"] = " --load-address 0x6800 "
    cfg["bootloader_file"] = "multi_boot_pw.170906_1033.bin"
    cfg["app_axf"] = "app.axf"
    cfg["app_bin"] = "app.bin"

    return (cfg)



def gen_config(args):
    print("Gen Config ", args)
    cfg = {}
    cfg["sys"] = gen_sys_cfg(args)
    cfg["prj"] = gen_prj_cfg()
    cfg["output"] = gen_output_cfg()

    if (0):
        for key in cfg:
            print (key,cfg[key])

    return (cfg)

def find_keil_prj_option(fname):

    # this function
    # 1. find the option from keil project file
    # 2. find the version from the gw_app_main.h file in keil project file

    keywords=[]
    keywords.append("<Define>AM_PACKAGE_BGA")
    keywords.append("</OutputName>")
    keywords.append("app_main.h</FilePath>")

    found_lines = find_lines(fname,keywords)

    option ={}
    for line in found_lines:
        if "AM_PACKAGE_BGA" in line:
            option["build_option"] = line.replace("<Define>","").replace("</Define>\n","")
            option["build_option"] = option["build_option"].strip()
        if "</OutputName>" in line:
            option["OutputName"] = line.replace("</OutputName>\n","").replace(" <OutputName>","")
            option["OutputName"] = option["OutputName"].strip()
        if "app_main.h" in line:
            option["app_main"] = line.replace("<FilePath>","").replace("</FilePath>\n","")
            option["app_main"] = option["app_main"].strip().replace("..\\","")

    keywords=[]
    keywords.append("#define BUILD_VERSION")
    fname = os.getcwd().replace("keil","")  + option["app_main"]
    found_lines = find_lines(fname,keywords)

    build_version = found_lines[0].replace("#define BUILD_VERSION", "").strip()
    build_version = build_version.replace("\r", "")
    build_version = build_version.replace("0x", "")

    keywords=[]
    keywords.append("#define MAJOR_VER")
    keywords.append("#define MINOR_VER")
    keywords.append("#define BUILD_NUM")
    keywords.append("#define RC_RELEASE")

    found_lines = find_lines(fname,keywords)

#    print (found_lines)
    _major = ""
    _minor = ""
    _build_num = ""
    _rc_num = ""
    for line in found_lines:
        line = line.replace("\r", "")
        line = line.replace("\n", "")
        line = line.replace("0x", "")
        if ("#define MAJOR_VER" in line):
            _major = int(line.replace("#define MAJOR_VER",""))
        elif ("#define MINOR_VER" in line):
            _minor = int(line.replace("#define MINOR_VER",""))
        elif ("#define BUILD_NUM" in line):
            _build_num = int(line.replace("#define BUILD_NUM",""))
        elif ("#define RC_RELEASE" in line):
            _rc_num = int(line.replace("#define RC_RELEASE",""))

    fw_version = "{:02d}{:02d}{:02d}".format(_major,_minor,_build_num+_rc_num)
#    print ("version",fw_version,_major,_minor,_build_num,_rc_num)


    if (len(build_version)==6):
        # Convert Hex to Decimal to match the display format
        major_ver = "{:02d}".format(int(build_version[:2], 16))
        minor_ver = "{:02d}".format(int(build_version[2:4], 16))
        build_num = "{:02d}".format(int(build_version[-2:], 16))

        print (major_ver,minor_ver,build_num)
        build_version = major_ver + minor_ver + build_num

        option["version"] = build_version

    option["version"] = fw_version
#    print (option["version"])

    return (option)

def gen_file(cfg):

    sys = cfg["sys"]
    prj = cfg["prj"]
    output = cfg["output"]

    fromelf = output["fromelf"]
    release_path = prj["path_local_release"]
    print (release_path,os.path.exists(release_path))
    if (os.path.exists(release_path) != True):
        print ("create folder ", release_path)
        os.makedirs(release_path)

    if (1):
        op_file = "{}.{}.v{}".format(prj["output_file"], prj["language_pack"], prj["option"]["version"])

        print ("Copy axf file '{}' to release folder '{}'".format(prj["build_file"],output["app_axf"]))
        src = "{}\\{}".format(prj["path_build"],prj["build_file"])
        dest = "{}\\{}".format(prj["path_local_release"],output["app_axf"])
        shutil.copyfile(src, dest)

        print ("Convert axf file '{}' to bin file '{}'".format(output["app_axf"],output["app_bin"]))

        # fromelf --bin --output bin\{}.bin bin\{}.axf
        cmd = "cd {} && {} --bin --output {} {}".format(prj["path_local_release"],output["fromelf"],output["app_bin"], output["app_axf"])
#        print (cmd)
        run_shell(cmd)

        print ("Generate TimeStamp output file {}.{}.bin".format(op_file,sys["timestamp"]))

        cmd = "cd {} && rm {}.*".format(prj["path_local_release"],op_file)
        run_shell(cmd)

        src = "{}\\{}".format(prj["path_local_release"],output["app_bin"])
        md5sum = md5(src)

        dest = "{}\\{}.{}.{}.bin".format(prj["path_local_release"],op_file,sys["timestamp"],md5sum[:8])
        shutil.copyfile(src, dest)


        dest = "{}\\{}.{}.bin".format(prj["path_local_release"],op_file,"000000_000000")
        shutil.copyfile(src, dest)

    if (0):
        print("---1", prj["option"]["version"], prj["language_pack"])
        print (sys["timestamp"])
        print (output["fromelf"])
        print (prj["path_local_release"])
        print (prj["path_build"])
        print (prj["build_file"])



    if (0):
        for key in cfg:
            print (key,cfg[key])

def test():
    print ("test")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--g', help='test config')
    parser.add_argument('--c', help='test config')
    parser.add_argument('--a', help='test config')
    parser.add_argument('--b', help='test config')
    parser.add_argument('--z', help='test config')
    parser.add_argument('--h', help='test config')
    parser.add_argument('--l', help='test config')
    parser.add_argument('--u', help='test config')
    parser.set_defaults(g="0")
    parser.set_defaults(a="app_main.h")
    parser.set_defaults(c="keil")
    parser.set_defaults(b="bl")
    parser.set_defaults(z="z")
    parser.set_defaults(h="")
    parser.set_defaults(l="en")
    parser.set_defaults(u="")

    args = parser.parse_args()
    git = args.g
    src = args.c
    app = args.a
    bl = args.b
    backup = args.z
    target = args.h
    language = args.l

    if (0):
        global python3
        global fromelf

        python3 = "C:\\python36\\python "
        fromelf = "C:\\Keil_v5\\ARM\\ARMCC\\bin\\fromelf.exe"

    #    test()

    cfg = gen_config(args)
    gen_file(cfg)


#    print ("backup project 170618",git,src,target,bl,language)

if __name__ == "__main__":
    main();